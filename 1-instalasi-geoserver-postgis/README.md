# Instalasi Geoserver dan PostGIS dalam satu jaringan

## 1. Instalasi GeoServer dan PostGIS 

### Instalasi Menggunakan Docker

#### 1. Jika belum ada, aktifkan Docker Swarm terlebih dahulu
```sh
docker swarm init
```

##### QnA :
- Apa arti script di atas ?
  > Mengaktifkan fitur dan membuat Docker Swarm yang memungkinkan kita mengelola dan memerintah Docker lintas server, seru ya ! 
  > Jika belum ada Docker Swarm sebelumnya maka script tersebut juga memasukan komputer ke Docker Swarm yang baru dibuat
- Mengapa pakai Docker Swarm ?
  > Karena sangat mungkin pengembangan map service terjadi di beberapa server sekaligus,
  > contohnya PostGIS yang dapat berada di server berbeda untuk instansi yang berbeda.
  > Dan Docker Swarm pun dapat berjalan di satu server saja seperti di laptop

#### 2. Jika belum ada, buat jaringan yang dapat digunakan bersama oleh berbagai container
```sh
docker network create -d overlay --attachable jaringan-bersama-geoserver-postgis
```

##### QnA :
- Mengapa harus menggunakan jaringan bersama ?
  > Agar GeoServer nantinya dapat langsung mengakses PostGIS 
- Mengapa menggunakan **-d overlay** ?
  > Agar jaringan tersebut dapat diakses oleh Docker Swarm yang Dockernya dapat tersebar di beberapa server berbeda
- Mengapa menggunakan **--attachable** ?
  > Agar container maupun container service lainnya dapat ikut bergabung di jaringan tersebut misalnya web service Express, Flask, Laravel, dsb.

#### 3. Buat folder project dan folder untuk menampung data PostGIS dan data GeoServer di dalam folder project tersebut 

Pada perintah di bawah ini nama folder projectnya **layanan-peta**, nama folder untuk data PostGIS nya **data-postgis** 
```sh
mkdir layanan-peta
cd layanan-peta
mkdir data-postgis
mkdir data-geoserver
```

#### 4. Bangun Docker Stack dari kombinasi GeoServer dan PostGIS 

Copy file [docker-compose ini](1-instalasi-geoserver-postgis/docker-compose.yml) ke folder project (layanan-peta), sehingga pada folder tersebut terdapat dua objek yaitu folder **data-postgis** dan file **docker-compose.yml**

Dari folder layanan-peta, eksekusi perintah di bawah ini
```sh
docker stack deploy --compose-file docker-compose.yml layanan-peta  
```

##### QnA :
- Apa arti script di atas ?
  > Membuat Docker Stack menggunakan konfigurasi dari docker-compose.yml
- Untuk apa Docker Stack itu ?
  > Docker Stack mengelola aplikasi dengan beberapa teknologi sekaligus yang berada di berbagai container yang berbeda yang bisa berada di server-server yang berbeda pula
  > Contohnya Docker Stack layanan-peta yang isinya ada container GeoServer dan container PostGIS
- Apa maksud **"5003:5432"** atau **"8003:8080"** pada konfigurasi port di docker-compose.yml
  > **"5003:5432"** artinya port 5432 di container PostGIS dapat diakses dari port 5003 komputer host (laptop / servermu),
  > karena secara bawaan, container Docker itu terisolir, agar bisa mengakses port di dalam container maka harus dihubungkan dulu ke host seperti ini
  > Begitupun **"8003:8080"** yang artinya port 8080 di container GeoServer dapat diakses dari port 8003 komputer host


### Instalasi di Windows

1. Install PostGIS mengikuti panduan [di sini](https://www.bostongis.com/PrinterFriendly.aspx?content_name=postgis_tut01)
2. Install GeoServer untuk Windows menggunakan panduan [di sini](https://docs.geoserver.org/latest/en/user/installation/win_installer.html)
  - Port yang dipilih dapat diganti dari 8080 dengan yang lebih unik seperti misalnya 8003, dsb. agar tidak bentrok dengan aplikasi lain yang mungkin ada di Windowsnya

## 2. Mengakses GeoServer 

Akses aplikasi GeoServer web melalui alamat **http://localhost:8003/geoserver/
> Username : admin
> Password : geoserver

## [Lanjut ke Membuat Workspace ➡️ ](../2-membuat-workspace/)
