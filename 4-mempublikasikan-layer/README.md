# Mempublikasikan Layer

## Apa Layer itu ? 

- Layer adalah representasi dari satuan peta dengan tipe yang sama
- Contohnya **Layer** jaringan listrik yang berisi beragam data jaringan listrik dengan tipe LineString 

## Publikasi Layer dari Data Store 

### 1. Login ke GeoServer

### 2. Klik menu **Layer**  

![Menu Layer di GeoServer](./klik-menu-layer.png)

### 3. Klik **Add new Layer**

![Add New Layer di GeoServer](./klik-add-new-layer.png)

### 4. Pilih layer dari **Store** **Latihan:postgis-koleksipeta-hutan**, pulih **Layer name** **hutan** lalu klik **Publish**

![Publish Layer dari PostGIS Database di GeoServer](./klik-publish-from-postgis-database-data-store.png)


### 5. Setelah klik **Publish** akan masuk ke halaman **Edit Layer** 

![Publish Layer Edit Layer di GeoServer](./edit-layer.png)

### 6. Hitung **Native Bounding Box** dan **Lat/Lon Bounding Box** secara otomatis dengan **compute from data** dan **Compute from native bounds**

![Hitung Bounding Box otomatis di Edit Layer di GeoServer](./edit-layer-bounding-box.png)

### 7. Klik **Save** 

![Save Edit Layer di GeoServer](./edit-layer-save.png)

### 8. Selesai akan dibawa ke halaman **Layers** dan muncul layer **hutan** 

![Selesai Save Edit Layer di GeoServer](./edit-layer-finished.png)

## Preview Layer

### 1. Klik menu **Layer Preview**  

![Klik Menu Layer Preview di GeoServer](./klik-layer-preview.png)

### 2. Pada data **hutan**, klik menu **OpenLayers** untuk menampilkan layer menggunakan OpenLayers 

![Layer Preview OpenLayers di GeoServer](./layer-preview-openlayers.png)

### 3. Setelah itu akan masuk ke tampilan preview **OpenLayers** 

![Layer Preview OpenLayers Display di GeoServer](./layer-preview-openlayers-display.png)
