# Membuat Store 

## Apa Store itu ? 

- Store adalah sumber data yang akan digunakan untuk publikasi layer peta 
- Store dapat digunakan untuk sumber data Vector maupun Raster 

## Mengakses Menu Store 

### 1. Login ke GeoServer
### 2. Pastikan sudah ada **Workspace** 

  ![Hasil Workspace di GeoServer](../2-membuat-workspace/hasil.png)

### 3. Klik menu **Stores**  

  ![Menu Store di GeoServer](./klik-menu-store.png)

## Membuat Store dari Sumber Data PostGIS
### 1. Persiapkan sumber data
  
Sampel sumber data untuk PostGIS dapat dibuat melalui query SQL ini: [hutan.sql](../sampel-data/hutan.sql)

### 2. Klik **Add new Store** 

  ![Add new Store di GeoServer](./klik-add-new-store.png)


### 3. Pilih **PostGIS database** 

  ![Pilih PostGIS database Add new Store di GeoServer](./pilih-postgis-database.png)

### 4. Masukan konfigurasi untuk **New Vector Data Source** - **PostGIS database** 

  ![Konfigurasi New Vector Data Source PostGIS di GeoServer](./isi-postgis-database-new-vector-data-source.png)

### 5. Klik **Save**

  ![Save New Vector Data Source PostGIS di GeoServer](./save-postgis-database-new-vector-data-source.png)

### 6. Setelah selesai Save, maka akan muncul tampilan **New Layer** 

  ![Selesai save New Vector Data Source PostGIS di GeoServer](./save-selesai.png)

## [Lanjut ke Mempublikasikan Layer ➡️ ](../4-mempublikasikan-layer)

## Membuat Store dari Sumber Data SHP

### 1. Persiapkan sumber data

Sampel sumber data SHP dapat diperoleh di sini: 
- [hutan.shp](../sampel-data/hutan.shp) 
- [hutan.dbf](../sampel-data/hutan.dbf) 
- [hutan.prj](../sampel-data/hutan.prj) 
- [hutan.shx](../sampel-data/hutan.shx) 
- [hutan.cpg](../sampel-data/hutan.cpg) 

### 2. Klik **Add new Store** 

  ![Add new Store di GeoServer](./klik-add-new-store.png)

## [Lanjut ke Mempublikasikan Layer ➡️ ](../4-mempublikasikan-layer)
