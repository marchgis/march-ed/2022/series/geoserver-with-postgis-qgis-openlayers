# GeoServer with PostGIS QGIS OpenLayers

## [1. Instalasi Geoserver dan PostGIS ➡️ ](1-instalasi-geoserver-postgis)
## [2. Membuat Workspace ➡️ ](2-membuat-workspace)
## [3. Membuat Store ➡️ ](3-membuat-store)
## [4. Mempublikasikan Layer ➡️ ](4-mempublikasikan-layer)
## [5. Menampilkan di OpenLayers ➡️ ](5-menampilkan-di-openlayers)

## Aneka Tips
- Mengekspor layer QGIS ke PostGIS dengan DB Manager

## Referensi

### Apa itu WMS, WFS, WMTS, WPS 
- [Web Map Service (WMS)](https://www.ogc.org/standards/wms)
- [Web Feature Service (WFS)](https://www.ogc.org/standards/wfs)
- [Web Map Tile Service (WMTS)](https://www.ogc.org/standards/wmts)
- [Web Processing Service (WPS)](https://www.ogc.org/standards/wps)
