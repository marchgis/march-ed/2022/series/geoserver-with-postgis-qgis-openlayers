# Membuat Workspace 

## Apa Workspace itu ? 

- Workspace adalah fitur untuk mengelompokkan sumber data dan layer peta yang memiliki keterkaitan penggunaan

## Cara Membuat Workspace

1. Login ke GeoServer
2. Klik menu **Workspaces** 

  ![Klik menu Workspaces](./klik-menu-workspace.png)

3. Klik **Add new Workspaces**

  ![Klik Add new Workspaces](./klik-add-new-workspace.png)

4. Isi form **New Workspace** lalu klik **Submit**

  ![Isi form New Workspace](./isi-form-new-workspace.png)

  - **Name** nya : Latihan
  - **Namespace URI** nya : latihan
  - Centang **Default Workspace**

5. Seperti ini hasilnya 

  ![Hasil Workspace](./hasil.png)

## [Lanjut ke Membuat Store ➡️ ](../3-membuat-store/)
